﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TShockAPI;
using Terraria;
using TerrariaApi.Server;
using Microsoft.Xna.Framework;

namespace NoVisualLimit
{
    [ApiVersion(2, 1)]
    public class NoVisualLimit : TerrariaPlugin
    {
        /// <summary>
        /// Gets the author(s) of this plugin
        /// </summary>
        public override string Author => "Leader";

        /// <summary>
        /// Gets the description of this plugin.
        /// A short, one lined description that tells people what your plugin does.
        /// </summary>
        public override string Description => "取消服务器的版本验证";

        /// <summary>
        /// Gets the name of this plugin.
        /// </summary>
        public override string Name => "NoVisualLimit";

        /// <summary>
        /// Gets the version of this plugin.
        /// </summary>
        public override Version Version => new Version(1, 0, 0, 0);

        /// <summary>
        /// Initializes a new instance of the NoVisualLimit class.
        /// This is where you set the plugin's order and perfrom other constructor logic
        /// </summary>
        public NoVisualLimit(Main game) : base(game)
        {

        }

        /// <summary>
        /// Handles plugin initialization. 
        /// Fired when the server is started and the plugin is being loaded.
        /// You may register hooks, perform loading procedures etc here.
        /// </summary>
        public override void Initialize()
        {
            ServerApi.Hooks.NetGetData.Register(this, OnNetGetData);
        }

        private void OnNetGetData(GetDataEventArgs args)
        {
            if (args.MsgID == PacketTypes.ConnectRequest)
            {
                args.Handled = true;
                if (Main.netMode != 2)
                {
                    return;
                }
                if (Main.dedServ && Netplay.IsBanned(Netplay.Clients[args.Msg.whoAmI].Socket.GetRemoteAddress()))
                {
                    NetMessage.TrySendData(2, args.Msg.whoAmI, -1, Lang.mp[3].ToNetworkText(), 0, 0f, 0f, 0f, 0, 0, 0);
                    return;
                }
                if (Netplay.Clients[args.Msg.whoAmI].State != 0)
                {
                    return;
                }
                /*if (!(args.Msg.reader.ReadString() == "Terraria" + 230))
                {
                    NetMessage.TrySendData(2, args.Msg.whoAmI, -1, Lang.mp[4].ToNetworkText(), 0, 0f, 0f, 0f, 0, 0, 0);
                    return;
                }*/
                if (string.IsNullOrEmpty(Netplay.ServerPassword))
                {
                    Netplay.Clients[args.Msg.whoAmI].State = 1;
                    NetMessage.TrySendData(3, args.Msg.whoAmI, -1, null, 0, 0f, 0f, 0f, 0, 0, 0);
                    return;
                }
                Netplay.Clients[args.Msg.whoAmI].State = -1;
                NetMessage.TrySendData(37, args.Msg.whoAmI, -1, null, 0, 0f, 0f, 0f, 0, 0, 0);
            }
        }

        /// <summary>
        /// Handles plugin disposal logic.
        /// *Supposed* to fire when the server shuts down.
        /// You should deregister hooks and free all resources here.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ServerApi.Hooks.NetGetData.Deregister(this, OnNetGetData);
                // Deregister hooks here
            }
            base.Dispose(disposing);
        }
    }
}