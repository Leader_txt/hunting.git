# Hunting

#### 介绍
A mini game plugin for TShock/TShock小游戏插件——Hunting

一款高自由度，可循环使用的吃鸡游戏插件

玩家只需一个简单指令即可在任何时候参加游戏

服主也只需要透过简单的指令/配置，设置游戏内容



游戏简介

游戏开始前，玩家需要透过/join指令加入游戏

待玩家数量达到指定人数后，并等待一段时间

游戏会进行初始化



游戏开始后，玩家会被传送到开始位置，

乘坐天空列车并随意选择地方跳落



游戏目的，击杀所有玩家并成为最后的生存者

本插件还提供了组队模式，让玩家们以团队方式活动



游戏进行时，会随机空投物资或导弹

每经过一定时间会进行缩圈！



直到生存者或组为一时，游戏便会结束！

地图建造要求以及插件注意事项​
1.在地图右上角侧建造一个封闭大厅，并在左侧修建一条横跨全服的铁轨
2.在地表随机放置一些箱子，用于生成物资
3.建议封闭地表所有通往地下的洞穴，否则玩家很难找到敌人
4.建议开启禁止破坏地图，永昼，无怪物
4968_83180f7b7395207fd9e18b0bda1f3be9.png

指令​

指令​
指令详解​
/join	参加本轮吃鸡游戏
/count	查看本轮吃鸡游戏情况
/over	强制结束本轮游戏
/set hall	设置玩家当前位置为游戏大厅（等待点）
/set reload	重读配置文件
/set start	设置玩家当前位置为游戏开始点
/set able	设置是否启动游戏插件
/set starters 人数	设置游戏最少参加人数
/set items add 物品id [数量] [前缀]	添加游戏初始物品
/set items del 物品id	删除游戏初始物品
/set items list	列出游戏初始物品
/set ran add 概率 物品id [数量] [前缀]	添加宝箱随机生成物品
/set ran del 物品id	删除宝箱随机生成物品
/set ran list	列出所有宝箱可随机生成的物品
/set ran edit 最小值 最大值	设置每个宝箱可生成的物品数量
/set drop 数量	设置每次最大空投数量


权限​

权限名称​
对应指令​
hunting.admin	对应除了/join，/count 外的所有指令


配置文件​

配置名称​
配置详解​
Hall	休憩/等待区位置，使用指令设置
Start	游戏开始位置，使用指令设置
Enable	是否启用本插件，True启用；false禁用
MaxJoinSec	达到指定人数后游戏开始前计时（秒）
Starters	最少参与游戏人数
Circle	索圈设定
Smaller - 缩圈百分比
Num - 缩圈次数
Break - 每次缩圈间隔时间（秒）
StartInv	游戏开始初始装备，使用指令设置
MaxItems	箱子内最多生成物品数量
MinItems	箱子内最少生成物品数量
MaxDrop	每次空投物品最大数量
DestroySize	轰炸区大小
DestroyTileY	轰炸从地图Y轴（高度）下降
StartLife	游戏初始生命值
UnSafeBuffs	进入毒圈时的buff，填写Buff ID
FalledBuffs	玩家死亡时的buff，填写Buff ID
RandomItems	设置随机空投物品，使用指令设置
